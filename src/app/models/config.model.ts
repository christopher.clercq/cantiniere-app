
export class Config {

    constructor(
        public limite: string,
        public maxPlats: Number,
        public id?: string) {
    }
}