import { Product } from './product.model';
import { User } from './user.model';
import { Plat } from './plat.model';
import { Jour } from './jour.model';

export class Order{
    constructor(
       
       public jour: Jour,
        public plat: Plat,
        public formule: boolean,
        public id?: string, 
        public user?: User,
        public date?: Date,
        public status?: Number){

    }


}