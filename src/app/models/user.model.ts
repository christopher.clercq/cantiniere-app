import { Role } from './role.model';

export class User {
    _id: string;
    firstName: string;
    lastName: string;
    password?: string;
    email: string;
    wallet: number;
    role: Role;
    token?: string
    constructor(

    ) {

    }
}