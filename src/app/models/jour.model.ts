import { Plat } from './plat.model';

export class Jour {

    constructor(
        public id: string,
        public name: string,
        public date?: Date,
        public one?: Plat,
        public second?: Plat,
        public orderable?: boolean) {
    }
}