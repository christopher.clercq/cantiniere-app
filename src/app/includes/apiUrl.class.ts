
export class ApiUrl {
   public static URL_PRODUCTS: string = 'http://localhost:3000/products';
   public static URL_PLATS_SEMAINE: string = 'http://localhost:3000/semaines';
   // public static URL_PLATS_SEMAINE: string = 'http://localhost:3003/';
   public static URL_TYPES: string = 'http://localhost:3000/types';
   public static URL_PLATS: string = 'http://localhost:3000/plats';
   public static URL_ORDERS: string = 'http://localhost:3000/orders';
   public static URL_AUTHENTICATE: string= 'http://localhost:3003/authenticate';
   public static URL_REGISTER: string= 'http://localhost:3003/register';
   public static URL_ACCOUNT: string= 'http://localhost:3003/api/account';
   public static URL_CONFIG: string= 'http://localhost:3000/configuration';
}
