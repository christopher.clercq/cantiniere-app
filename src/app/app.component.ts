import { Component } from '@angular/core';
import { MDBModalService, MDBModalRef } from 'angular-bootstrap-md';
import { LoginModalComponent } from './components/modals/login-modal/login-modal.component';
import { DeconnexionComponent } from './components/modals/deconnexion/deconnexion.component'
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { User } from './models/user.model';
import { LoaderService } from './services/utils/loader.service';
import { NavFilterService } from './services/nav-filter.service';
import { NotificationService } from './services/utils/notification.service';
import { PanierService } from './services/panier.service';
import { ConfigurationService } from './services/configuration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private currentUser: User;
  public load: boolean;
  public cartState: any;
  private logoutModalRef: MDBModalRef;

  constructor(private modalService: MDBModalService,
    public authenticationService: AuthenticationService,
    private loaderService: LoaderService,
    private notificationService: NotificationService,
    private panierService: PanierService,
    private router:Router
  ) {
  }

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe(u => {
      this.currentUser = u
    });
    
    this.loaderService.load.subscribe(l => this.load = l);
    this.panierService.cartState.subscribe(cart => this.cartState = cart);
  }

  openLoginModal() {
    this.modalService.show(LoginModalComponent);
  }

  openLogoutModal() {
    this.logoutModalRef = this.modalService.show(DeconnexionComponent, {
      backdrop: false,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: '',
      containerClass: '',
      animated: true
    });

    this.logoutModalRef.content.action.subscribe((result: boolean) => {
      if (result) {
        this.authenticationService.logout();
        this.router.navigate(['/']);
      }
    });
    
  }

}
