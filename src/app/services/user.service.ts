import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/user.model'
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`http://localhost:3003/api/user`, { withCredentials: true })
            .pipe(map(
                data => {
                    return data;
                }
            ))
    }

    get(id: string): Observable<User> {
        return this.http.get<User>(`http://localhost:3003/api/user/${id}`, { withCredentials: true })
            .pipe(map(
                user => {
                    delete user.password;
                    return user;
                }
            ))
    }

    delete(id: string): Observable<any> {
        return this.http.delete<User>(`http://localhost:3003/api/user/${id}`, { withCredentials: true })
    }
}