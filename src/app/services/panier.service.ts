import { Injectable } from '@angular/core';
import { Order } from '../models/order.model';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  private cartSubject : BehaviorSubject<any>;
  private Orders: Order;
  private total = 0;
  public cartState : Observable<any>;

  constructor() { 
    this.cartSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('basket')));
    this.cartState = this.cartSubject.asObservable();
  }


  addOrder(order: Order) {    
      const oldItems = JSON.parse(localStorage.getItem('basket')) ? JSON.parse(localStorage.getItem('basket')) : [];
     oldItems.push(order);
     localStorage.setItem('basket', JSON.stringify(oldItems));
     this.refreshCart();
  }

  getTotal(){
    this.refreshCart();
    return this.total;
  }

  refreshCart(){
    if(localStorage.getItem('basket')){
      const basket = JSON.parse(localStorage.getItem('basket'));
      basket.forEach(order => {
        this.total += order.formule == 1 ? 7 : 10;
      })
      this.cartSubject.next(basket);
    }
  }

  removeItem(){
    localStorage.removeItem('basket');
    this.cartSubject.next(null);
  }

}
