import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from 'src/app/models/user.model';
import { ApiUrl } from '../includes/apiUrl.class';
import * as moment from "moment";
import * as jwtDecode from 'jwt-decode';
import { NotificationService } from './utils/notification.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Order } from '../models/order.model';
import { CookieService } from 'ngx-cookie-service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient, private notService: NotificationService,
        private cookieService: CookieService) {
        this.currentUserSubject = new BehaviorSubject<User>(this.decodedToken);
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        if (this.isLogged) {
            return this.currentUserSubject.value;
        }
    }

    login(email: string, password: string) {

        return this.http.post<any>(`${ApiUrl.URL_AUTHENTICATE}`, { email, password }, { withCredentials: true })
            .pipe(map(res => {
                // login successful if there's a jwt token in the response
                if (res.token) {
                    this.setSession(res);
                    return;
                }
            }))
    }

    private setSession(authResult: any) {
        const expiresAt = moment().add(authResult.expiresIn, 'second');
        localStorage.setItem('t', authResult.token);
        localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));

        this.currentUserSubject.next(this.decodedToken);
        this.notService.success(`Heureux de te voir ici ${this.decodedToken.firstName}`, `Bienvenue`)
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('t');
        localStorage.removeItem("expires_at");
        this.cookieService.delete('token');
        this.currentUserSubject.next(null);
    }

    get isLogged() {
        return localStorage.getItem('t') && moment().isBefore(this.getExpiration());
    }

    getExpiration() {
        const expiration = localStorage.getItem("expires_at");
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }

    get isAdmin() {
        return this.isLogged && this.currentUserSubject.value.role === 'Admin';
    }

    get decodedToken(): User {
        const helper = new JwtHelperService();
        return helper.decodeToken(localStorage.getItem('t'));
    }

    update(user: User) {
        return this.http.put<User>(`${ApiUrl.URL_ACCOUNT}`, user, { withCredentials: true })
            .pipe(map(
                () => {
                    return;
                }
            ))
    }

    register(user: User) {
        return this.http.post<User>(`${ApiUrl.URL_REGISTER}`, user)
            .pipe(map(
                success => {
                    this.login(user.email, user.password)
                        .subscribe(
                            success => {
                                return success
                            }
                        )
                }
            ))
    }
}