import { TestBed } from '@angular/core/testing';

import { platsSemaineService } from './plats-semaine.service';

describe('PlatsSemaineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: platsSemaineService = TestBed.get(platsSemaineService);
    expect(service).toBeTruthy();
  });
});
