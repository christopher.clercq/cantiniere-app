import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../includes/apiUrl.class'
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import * as momentTz from 'moment-timezone'

import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Jour } from '../models/jour.model';
import { Config } from '../models/config.model';
@Injectable({
  providedIn: 'root'
})

export class ConfigurationService {

  constructor(private http: HttpClient) {
  }

  /**
   * Update the configuration
   * @param limite hours limite for every day
   * @param maxPlats maximum of order by day
   */
  update(config: Config):Observable<any>{
    return this.http.put<any>(`${ApiUrl.URL_CONFIG}/1`,config)
  }

 get(){
    return this.http.get<Config>(`${ApiUrl.URL_CONFIG}/1`).pipe(map(
      config => {
        return config;
      }
    ))
  }
}
