import { map } from 'rxjs/operators';
import { Product } from './../models/product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiUrl } from "../includes/apiUrl.class";
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  // Constructor
  constructor(private http: HttpClient) { }

  // find all products
  findAll(): Observable<Product[]> {
    return this.http.get<Product[]>(`localhost:3003/products`)
      .pipe(map(data => {
        return data;
      }));
  }

  getProduct(id: number) {
    return null;
  }

  /**
   * add a new product
   * @param product 
   */
  add(product: Product) {
    return this.http.post<Product>(`${ApiUrl.URL_PRODUCTS}`, product)
      .pipe(map(product => {
        return product;
      }));
  }

  update(product: Product) {
    return this.http.put(`${ApiUrl.URL_PRODUCTS}/${product.id}`, product)
      .pipe(map(product => {
        return product;
      }));
  }

  // delete a product 
  delete(id: string) {
    return this.http.delete(`${ApiUrl.URL_PRODUCTS}/${id}`)
      .pipe(map(data => {
        return data;
      }))
  }

  // get all product organized by type (for basic user)
  getAllOrganized(): Observable<Product[]> {
    var arrayProduct: Array<any> = [];
    return this.http.get<Product[]>(`${ApiUrl.URL_PRODUCTS}`)
      .pipe(map(data => {
        Object.keys(data).forEach(index => { // Pour chaque Object, on parcour et on a la "key"
          const type = data[index].type.id; // Type = nom du type de l'objet
          arrayProduct[type] == undefined ? arrayProduct[type] = [] : ''; // Si le tableau (ex: arrayProduct['Boissons']) existe : rien, sinon : le créer
          data[index].type.id == type ? arrayProduct[type].push(data[index]) : ''; // 
        });
        return arrayProduct;
      }));
  }

  // get the list of product types
  getTypes() {
    return this.http.get<Object[]>(ApiUrl.URL_TYPES)
      .pipe(map(data => {
        return data;
      }));
  }

  // get products by there type (bo cantiniere)
  getByType(id: string) {
    return this.http.get<Product[]>(`${ApiUrl.URL_PRODUCTS}?type.id=${id}`)
      .pipe(map(data => {
        return data;
      }));
  }

}
