import { map } from 'rxjs/operators';
import { Product } from './../models/product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiUrl } from "../includes/apiUrl.class";
import { Jour } from '../models/jour.model';
import { Order } from '../models/order.model';
import { PanierService } from './panier.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  add(orders: Order):Observable<any>{
    return this.http.post<Order>(`${ApiUrl.URL_ORDERS}`, orders);
  };


  get(jourId: string): Observable<Order> {
    return this.http.get<Order>(`${ApiUrl.URL_ORDERS}?jour.id=${jourId}`)
    .pipe(map(order => {
      return order
    }));
  }

  allByJour(jourId: string): Observable<Order[]> {
    return this.http.get<Order[]>(`${ApiUrl.URL_ORDERS}?jour.id=${jourId}&status=1`)
    .pipe(map(orders => {
      return orders
    }));
  }

  find(): Observable<Order[]> {
    return this.http.get<Order[]>(`${ApiUrl.URL_ORDERS}`)
    .pipe(map(orders => {
      return orders
    }));
  }

  update(order: Order){
    return this.http.put<Order>(`${ApiUrl.URL_ORDERS}/${order.id}`,order)
    .pipe(map(res => {
      return res
    }));
  }

  delete(id:string):Observable<any>{
    return this.http.delete<any>(`${ApiUrl.URL_ORDERS}/${id}`)
      .pipe(map(
        res => {
          return;
        }
      ))
  }


  
}
