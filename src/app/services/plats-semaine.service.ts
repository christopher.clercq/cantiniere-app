import { map } from 'rxjs/operators';
import { Product } from './../models/product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiUrl } from "../includes/apiUrl.class";
import { Jour } from '../models/jour.model';
import * as moment from 'moment';
import * as momentTz from 'moment-timezone';
import 'moment/locale/fr';
moment.locale('fr');
momentTz.locale('fr');
import { ConfigurationService } from './configuration.service';
@Injectable({
  providedIn: 'root'
})
export class platsSemaineService {

  public heureLimite: Observable<Date>;
  private heureSubject: BehaviorSubject<Date>;
  // Constructor
  constructor(private http: HttpClient, private config: ConfigurationService) { 
    // SCRIPT UPDATE DATE
    this.updateDate(); 
  }

  /**
   * Met a jour le / les plats du jour
   * 
   * @param plat 
   * @param jour 
   */
  update(jour: Jour) {
    return this.http.put<Jour>(`${ApiUrl.URL_PLATS_SEMAINE}/${jour.id}`, jour)
      .pipe(map(jour => {
        return jour;
      }));
  }

  /**
   * return la semaine avec les plats de la semaine.
   */
  find(): Observable<Jour[]> {
    return this.http.get<Jour[]>(ApiUrl.URL_PLATS_SEMAINE)
      .pipe(map(data => {
        return data;
      }));
  }

  updateDate(){
    var array:Array<Jour>;
    var limite: String;
    this.find().subscribe(
      jours => {
        array = jours;
        this.config.get().subscribe(
          config => {
            limite = config.limite
            array.forEach(jour => {
              jour.date = momentTz(`${jour.name} 00:00`,'ddd dddd HH:mm').tz('Europe/Paris').toDate();
              let old = jour.orderable;
              jour.orderable =  momentTz(`${jour.name}`,'ddd dddd').tz('Europe/Paris').isSame(momentTz().tz('Europe/Paris'),'day') && momentTz().tz('Europe/Paris').isBefore(momentTz(`${limite}`,'HH:mm').tz('Europe/Paris'))
              if(old !== jour.orderable){
               this.update(jour)
                 .subscribe(() => {});
              }
            })
          }
        )
      });


  }
}
