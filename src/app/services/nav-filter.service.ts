import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators';

import { Jour } from '../models/jour.model';
import { ApiUrl } from "../includes/apiUrl.class";
import { CachedSource } from 'webpack-sources';

@Injectable({
  providedIn: 'root'
})
export class NavFilterService {

  constructor(private http : HttpClient) { } 

  get():Observable<Jour[]>{
    return this.http.get<Jour[]>(ApiUrl.URL_PLATS_SEMAINE)
      .pipe(map(data => {
        return data;
      }));
  }

  getProductTypes():Observable<Object[]>{
    return this.http.get<Object[]>(ApiUrl.URL_TYPES)
      .pipe(map(data => {
        return data;
      }));
  }
}
