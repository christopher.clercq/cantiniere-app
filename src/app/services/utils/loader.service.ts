import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {


  private currentLoadSubject: BehaviorSubject<boolean>;
  public load: Observable<boolean>;

  constructor() {
      this.currentLoadSubject = new BehaviorSubject<boolean>(false);
      this.load = this.currentLoadSubject.asObservable();
   }

  start(){
    this.currentLoadSubject.next(true)
    return this.inLoad;
  }
  
   stop(){
    this.currentLoadSubject.next(false)
    return this.inLoad;
   }

   get inLoad(){
    return this.currentLoadSubject.value;
   }
   
}
