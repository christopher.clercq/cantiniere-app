import { Injectable } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Title } from '@angular/platform-browser';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public position = {
  topRight:'top-right ',
  topCenter:'top-center',
    topLeft:'top-left',
    topFullWidth:'top-full-width',
    bottomRight:'bottom-right',
    bottomCenter:'bottom-center',
    bottomLeft:'bottom-left',
    bottomFullWidth:'bottom-full-width'
  }
  constructor(public toastr: ToastrManager) { }

  error(text: string = 'Notification', title: string = 'Toast', options: object = {position: 'top-left'}) {
    this.toastr.errorToastr(text, title, options);
  }

  success(text: string = 'Notification', title: string = 'Toast', options: object = {position: 'top-left'}) {
    this.toastr.successToastr(text, title, options);
  }

  info(text: string = 'Notification', title: string = 'Toast', options: object = {position: 'top-left'}) {
    this.toastr.infoToastr(text, title, options);
  }

  warning(text: string = 'Notification', title: string = 'Toast', options: object = {position: 'top-left'}) {
    this.toastr.warningToastr(text, title, options);
  }

}
