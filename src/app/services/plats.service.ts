import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

// MINE 
import { ApiUrl } from '../includes/apiUrl.class'
import { Plat } from '../models/plat.model';

@Injectable({
  providedIn: 'root'
})
export class PlatsService {

  constructor(private http: HttpClient) { }

  get():Observable<Plat[]>{
    return this.http.get<Plat[]>(ApiUrl.URL_PLATS)
      .pipe(map(data => {
        return data;
      }));
  }

  add(plat: Plat):Observable<Plat>{
    return this.http.post<Plat>(ApiUrl.URL_PLATS, plat)
      .pipe(map(plat =>
        {
          return plat;
        }))
  }
}
