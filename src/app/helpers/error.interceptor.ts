import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from '../services/utils/notification.service';
import { MDBModalService, MDBModalRef } from 'angular-bootstrap-md';
import { LoginModalComponent } from '../components/modals/login-modal/login-modal.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    private loginModalRef = MDBModalRef;
    constructor(private authenticationService: AuthenticationService,
        private notService: NotificationService,
        private modalService: MDBModalService,
       ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if ([401, 403].indexOf(err.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                this.authenticationService.logout();
            }
           
            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}