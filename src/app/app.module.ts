import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule, NgbTabsetConfig} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PlatsSemaineComponent } from './components/plats-semaine/plats-semaine.component';
import { CarteComponent } from './components/carte/carte.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { RecapCommandeComponent } from './components/recap-commande/recap-commande.component';
import { PanierComponent } from './components/panier/panier.component';
import { AccountComponent } from './components/account/account.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';
import { NavFilterComponent } from './components/nav-filter/nav-filter.component';
import { LoginModalComponent } from './components/modals/login-modal/login-modal.component';

import { ForgetModalComponent } from './components/modals/forget-modal/forget-modal.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteModalComponent } from './components/modals/delete-modal/delete-modal.component';
import { PlatJourComponent } from './components/plat-jour/plat-jour.component';
import { OrderModalComponent } from './components/modals/order-modal/order-modal.component';
import { from } from 'rxjs';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { AdminComponent } from './components/admin/admin.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import { LoaderComponent } from './components/utils/loader/loader.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { TitleComponent } from './components/title/title.component';
import { DeconnexionComponent } from './components/modals/deconnexion/deconnexion.component'
import { CookieService } from 'ngx-cookie-service';
import { UserModalComponent } from './components/modals/user-modal/user-modal.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    PlatsSemaineComponent,
    CarteComponent,
    RegisterFormComponent,
    RecapCommandeComponent,
    PanierComponent,
    AccountComponent,
    ConfigurationComponent,
    NavFilterComponent,
    LoginModalComponent,
    ForgetModalComponent,
    DeleteModalComponent,
    PlatJourComponent,
    OrderModalComponent,
    AdminComponent,
    LoaderComponent,
    TitleComponent,
    DeconnexionComponent,
    UserModalComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  exports:[
    
  ],
  entryComponents:[LoginModalComponent, ForgetModalComponent, DeleteModalComponent, OrderModalComponent, DeconnexionComponent, UserModalComponent],
  bootstrap: [AppComponent],
  providers: [
    { provide: LOCALE_ID, useValue: "fr-Fr" },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    CookieService
  ]
})
export class AppModule { }
