import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nav-filter',
  templateUrl: './nav-filter.component.html',
  styleUrls: ['./nav-filter.component.scss']
})
export class NavFilterComponent implements OnInit {

  @Input() navInput:Array<Object>;
  @Output() selectFilter = new EventEmitter<string>();  // <!-- Voici l'output
  private activeItem:number = null;
  constructor() { }
  
  ngOnInit() {
    
  }

  onSelect(item:any,index:number){
    this.activeItem = index;
    this.selectFilter.emit(item); // Emet l'output en passant l'id de categorie
  }

}
