import { Component, OnInit } from '@angular/core';
import { NavFilterService } from 'src/app/services/nav-filter.service';
import { ProductService } from 'src/app/services/product.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Product } from 'src/app/models/product.model';

import { DeleteModalComponent } from '../modals/delete-modal/delete-modal.component'
import { MDBModalRef, MDBModalService } from 'angular-bootstrap-md';
import { Types } from 'src/app/models/types.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/user.model';
import { LoaderService } from 'src/app/services/utils/loader.service';
import { forkJoin } from 'rxjs';
import { NotificationService } from 'src/app/services/utils/notification.service';
@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.scss']
})
export class CarteComponent implements OnInit {

  private types: Array<Object>;
  private products: Array<Product>;

  private grouppedProducts: Array<Product>;

  private addProductForm: FormGroup;

  public show: boolean = false;
  public rd: number = -1;
  private submitted = false;
  private choosedType: Types;
  private deleteModalRef: MDBModalRef;
  public currentUser: User;

  constructor(
    private navFilterService: NavFilterService,
    private productsService: ProductService,
    private formBuilder: FormBuilder,
    private modalService: MDBModalService,
    public authenticationService: AuthenticationService,
    private loadService: LoaderService,
    private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.show = !this.loadService.start();

    // form builder
    this.addProductForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z-éàè^'+ ]{1,50}$/), Validators.minLength(3)]),
      price: new FormControl('', [Validators.required, Validators.pattern(/^[0-9.]{1,10}$/)]),
    })

    // session user
    this.authenticationService.currentUser.subscribe(u => {
      this.show = !this.loadService.start();
      this.currentUser = u;
      forkJoin(
        this.navFilterService.getProductTypes(),
        this.productsService.getAllOrganized()
      ).subscribe(
        ([types, products = null]) => {
          this.types = types;
          this.products = this.authenticationService.isAdmin ? null : products;
          this.show = !this.loadService.stop();
        }
      )
    });

  }

  /**
   * convenience getter for easy access to form fields
  */
  get f() { return this.addProductForm.controls; }

  loadTypes() {
    this.navFilterService.getProductTypes()
      .subscribe(types => {
        this.types = types;
        if (this.authenticationService.isAdmin) {
          this.show = !this.loadService.stop()
        }
      });
  }

  loadProducts() {
    if (!this.authenticationService.isAdmin) {
      this.productsService.getAllOrganized()
        .subscribe(products => {
          this.products = products;
          this.show = !this.loadService.stop()
        })
    }
  }
  /**
   * add a product
   */
  onAddProduct() {
    this.submitted = true;
    if (!this.addProductForm.valid) {
      return;
    }
    let found = false;
    this.grouppedProducts.forEach(
      product => {
        if (product.name.toLocaleUpperCase() == this.f.name.value.toLocaleUpperCase()) {
          found = true;
        }
      }
    )
    
    if(found){
       this.notificationService.error('L\' article existe déjà', 'erreur');
      return;
    }

    const newProduct: Product = this.addProductForm.value;
    const type: Types = { id: this.choosedType.id, name: this.choosedType.name, img: this.choosedType.img }
    newProduct.type = type;

    this.productsService.add(newProduct)
      .subscribe(
        product => {
          this.grouppedProducts.push(product);
          this.notificationService.success('Le produit a bien été ajouté', 'Succés');
          this.addProductForm.reset();
          this.submitted = false;
        }
      );
  }

  /**
   * delete a product by its id
   * @param productId id
   * @param index index
   */
  onDeleteProduct(productId: string, index: number) {
    this.deleteModalRef = this.modalService.show(DeleteModalComponent, {
      backdrop: false,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: '',
      containerClass: '',
      animated: true
    });

    this.deleteModalRef.content.action.subscribe((result: boolean) => {
      if (result) {
        this.productsService.delete(productId)
          .subscribe(
            data => {
              this.grouppedProducts.splice(index, 1);
              this.notificationService.success('Le produit a bien été supprimé', 'Succés')
            }
          );
      }
    });
  }

  /**
   * load the products by the type
   * @param type type of product
   */
  setFilter(type: Types) {
    this.loadService.start();
    this.rd = -1;
    this.choosedType = type;
    this.productsService.getByType(type.id)
      .subscribe(
        data => {
          this.grouppedProducts = data;
          this.loadService.stop();
        }
      );
  }

  /**
   * update a product
   * @param product the product
   * @param index the index in the array of product
   */
  updateItem(product: Product, index: number) {
    if (this.rd === index) {
      var editForm: FormGroup;

      editForm = this.formBuilder.group({
        name: new FormControl(product.name, [Validators.required, Validators.pattern(/^[a-zA-Z-éàè^'+ ]{1,50}$/), Validators.minLength(3)]),
        price: new FormControl(product.price, [Validators.required, Validators.pattern(/^[0-9.]{1,10}$/)]),
      })

      if (!editForm.valid) {
        this.notificationService.error('Les informations du produits ne sont pas valides', 'Erreur');
        return;
      }
      this.productsService.update(product)
        .subscribe(
          product => {
            this.rd = -1;
            this.notificationService.success('Le produit a été modifié', 'Succés');
          }
        )

    } else {
      this.rd = index;
    }
  }

  /**
   * return if a item is in edition
   * @param index the index of the produc
   */
  inEdit(index: number): boolean {
    return this.rd === index ? true : false;
  }

}

