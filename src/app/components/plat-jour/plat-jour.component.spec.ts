import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatJourComponent } from './plat-jour.component';

describe('PlatJourComponent', () => {
  let component: PlatJourComponent;
  let fixture: ComponentFixture<PlatJourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatJourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatJourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
