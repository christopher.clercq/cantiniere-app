import { Component, OnInit, Input } from '@angular/core';
import { Jour } from 'src/app/models/jour.model';
import { Plat } from 'src/app/models/plat.model';
import { Form, FormsModule, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { platsSemaineService } from 'src/app/services/plats-semaine.service';
import { OrderModalComponent } from '../modals/order-modal/order-modal.component';
import { MDBModalService } from 'angular-bootstrap-md';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/user.model';
import { NotificationService } from 'src/app/services/utils/notification.service';

@Component({
  selector: 'app-plat-jour',
  templateUrl: './plat-jour.component.html',
  styleUrls: ['./plat-jour.component.scss']
})
export class PlatJourComponent implements OnInit {

  private changePlatForm : FormGroup;
  @Input() jour:Jour;
  @Input() platsList: Array<Plat>;
  private currentUser: User;

  constructor(private formBuilder: FormBuilder,
    private platSemaineService: platsSemaineService,
    private modalService:MDBModalService,
    public authenticationService: AuthenticationService,
    private notService: NotificationService
    ) { }
    
  ngOnInit() {
    this.changePlatForm = this.formBuilder.group({
      platOne: new FormControl('', [Validators.required]),
      platTwo: new FormControl('', [Validators.required]),
    })
    this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
  }

  changePlat(){

    this.changePlatForm.value.platOne != '' ? this.jour.one = this.platsList[this.changePlatForm.value.platOne] : '';
    this.changePlatForm.value.platTwo != '' ? this.jour.second = this.platsList[this.changePlatForm.value.platTwo] : '';

    this.platSemaineService.update(this.jour)
      .subscribe(
        success => {

        },
        err => {
          
        }
      );
  }

  order(jour: Jour){
    if(!jour.orderable){
      this.notService.error('Vous ne pouvez commander pour ce jour actuellement','Oups ! ');
      return;
    }
    const orderModal =  this.modalService.show(OrderModalComponent, {
      backdrop: false,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'modal-lg',
      containerClass: '',
      animated: true,
      data:{
        jour: jour
      }
    });
  }
}
