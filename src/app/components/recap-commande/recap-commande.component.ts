import { Component, OnInit } from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { NavFilterComponent } from '../nav-filter/nav-filter.component';
import { NavFilterService } from 'src/app/services/nav-filter.service';
import { Jour } from 'src/app/models/jour.model';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/models/order.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { userInfo } from 'os';
import { NotificationService } from 'src/app/services/utils/notification.service';


@Component({
  selector: 'app-recap-commande',
  templateUrl: './recap-commande.component.html',
  styleUrls: ['./recap-commande.component.scss'],
})
export class RecapCommandeComponent implements OnInit {

  public semaine: Array<Jour> = [
    {
      id: "1",
      name: "Lundi"
    }, {
      id: "2",
      name: "Mardi"
    }, {
      id: "3",
      name: "Mercredi"
    }, {
      id: "4",
      name: "Jeudi"
    }, {
      id: "5",
      name: "Vendredi"
    },
  ];
  private searchDay: Jour;
  public orders: Array<Order>;

  constructor(config: NgbTabsetConfig,
    private orderService: OrderService,
    private authentificationService: AuthenticationService,
    private userService: UserService, 
    private notificationService:NotificationService) {

    // customize default values of tabsets used by this component tree
    config.justify = 'center';
    config.type = 'pills';
  }

  setFilter(event: Jour) {
    this.searchDay = event;
    this.searchOrders(event.id);
  }

  searchOrders(id: string) {
    this.orderService.allByJour(id)
      .subscribe(
        orders => {
          this.orders = orders;
        }
      )
  }

  private validateOrder(order: Order, index:number) {

    const price = order.formule ? 7 : 10;
    var user: User;
    
    this.userService.get(order.user._id)
      .subscribe(
        data => {
          user = data;
          if (user.wallet >= price) {
            user.wallet = user.wallet - price;
            this.authentificationService.update(user)
              .subscribe(
                res => {
                  order.status = 2;
                  this.orderService.update(order)
                    .subscribe(
                      res => {
                        this.orders.splice(index,1);
                        this.notificationService.info('Commande traitée', 'info');
                      }
                    )
                }
              )
          }

        }
      )
  }

  ngOnInit() {

  }

  deleteOrder(id:string, index:number){
    this.orderService.delete(id)
      .subscribe(
        () => {
          this.orders.splice(index,1);
          this.notificationService.info('Commande supprimée', 'info');
        }
      )
  }

}
