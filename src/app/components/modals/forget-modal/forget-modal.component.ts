import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';

@Component({
  selector: 'app-forget-modal',
  templateUrl: './forget-modal.component.html',
  styleUrls: ['./forget-modal.component.scss']
})
export class ForgetModalComponent implements OnInit {

  constructor(public modalRef: MDBModalRef) { }

  ngOnInit() {
  }

}
