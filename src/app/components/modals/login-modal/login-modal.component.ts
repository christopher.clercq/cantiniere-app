import { Component, OnInit } from '@angular/core';

import { MDBModalRef } from 'angular-bootstrap-md';
import { MDBModalService } from 'angular-bootstrap-md';
import { ForgetModalComponent } from '../forget-modal/forget-modal.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { first } from 'rxjs/operators';
import { NotificationService } from 'src/app/services/utils/notification.service';
@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;

  constructor(
    private modalRef: MDBModalRef,
    private modalService: MDBModalService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    public notService: NotificationService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  login() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .subscribe(
        () => {
          this.hide();
        },
        error => {
          this.notService.error('Authentification refusé','Erreur');
        }
      )
  }

  openForgetModal() {
    const forgetModal = this.modalService.show(ForgetModalComponent);
  }
  
  hide(){
    this.modalRef.hide();
  }
}
