import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Jour } from 'src/app/models/jour.model';
import { InputsModule, ButtonsModule } from 'angular-bootstrap-md'
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Order } from 'src/app/models/order.model';
import { User } from 'src/app/models/user.model';
import { OrderService } from 'src/app/services/order.service';
import { PanierService } from 'src/app/services/panier.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import * as moment from 'moment';
import 'moment/locale/fr';
moment.locale('fr');

@Component({
  selector: 'app-order-modal',
  templateUrl: './order-modal.component.html',
  styleUrls: ['./order-modal.component.scss']
})
export class OrderModalComponent implements OnInit {

  public jour: Jour;
  public doOrderForm: FormGroup
  constructor(
    public modalRef: MDBModalRef,
    private formBuilder: FormBuilder,
    private panierService: PanierService,
    private configService: ConfigurationService) { }

  ngOnInit() {
    this.doOrderForm = this.formBuilder.group({
      plat: new FormControl(null, [Validators.required]),
      formule: new FormControl(null, [Validators.required]),
    })
  }
  
  get f() { return this.doOrderForm.controls; }

  onSubmit(){
    // stop here if form is invalid
    if (this.doOrderForm.invalid || !this.jour.orderable ) {
       return };
    const formule = this.doOrderForm.value.formule;
    const plat = this.doOrderForm.value.plat ? this.jour.one : this.jour.second;
    
    this.panierService.addOrder({'jour':this.jour,'plat':plat,'formule':formule})

    this.modalRef.hide();
  }
}
