import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-deconnexion',
  templateUrl: './deconnexion.component.html',
  styleUrls: ['./deconnexion.component.scss']
})
export class DeconnexionComponent implements OnInit {

  action: Subject<any> = new Subject();
  
  constructor(public modalRef: MDBModalRef) { }

  ngOnInit() {
  }

  confirmDelete(){
    this.action.next(true);
    this.modalRef.hide();
  }

  abortDelete(){
    this.action.next(false);
    this.modalRef.hide();
  }

}
