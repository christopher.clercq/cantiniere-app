import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/utils/notification.service';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {

  @Input() id: string;
  updatedUser: Subject<any> = new Subject();

  private user: User;
  updateForm: FormGroup;
  public show:boolean = false;

  constructor(
    private userService:UserService,
    private formBuilder: FormBuilder,
    private authenticateService:AuthenticationService,
    private notificationService:NotificationService,
    private modalRef: MDBModalRef
  ) { }

  ngOnInit() {

    this.userService.get(this.id)
      .subscribe(
        user => {
          this.user = user;
          this.updateForm = this.formBuilder.group({
            email: [this.user.email, [Validators.email]],
            password: [null],
            wallet: [this.user.wallet]
          });
          this.show = true;

        }
      )
  }

  get f() { return this.updateForm.controls; }

  updateUser(){
    if (this.updateForm.invalid) {
      return;
    }

    this.user.email = this.f.email.value;
    this.user.password = this.f.password.value;
    this.user.wallet = this.f.wallet.value;
    this.f.password.value ? this.user.password = this.f.password.value : delete this.user.password;

    this.authenticateService.update(this.user)
      .subscribe(
        () => {
          this.notificationService.info('Les changements ont été effectué','Succés');
          this.updatedUser.next(this.user);
          this.modalRef.hide();
        }
      )
  }
}
