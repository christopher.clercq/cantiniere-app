import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {

  action: Subject<any> = new Subject();
  
  constructor(public modalRef: MDBModalRef) { }

  ngOnInit() {
  }

  confirmDelete(){
    this.action.next(true);
    this.modalRef.hide();
  }

  abortDelete(){
    this.action.next(false);
    this.modalRef.hide();
  }
}