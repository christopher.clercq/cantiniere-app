
import { Jour } from './../../models/jour.model';
import { Component, OnInit } from '@angular/core';
import { PlatsService } from 'src/app/services/plats.service';
import { Plat } from 'src/app/models/plat.model';
import { platsSemaineService } from 'src/app/services/plats-semaine.service'
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoaderService } from 'src/app/services/utils/loader.service';
import { NotificationService } from 'src/app/services/utils/notification.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { forkJoin } from 'rxjs';
import { Config } from 'src/app/models/config.model';

@Component({
  selector: 'app-plats-semaine',
  templateUrl: './plats-semaine.component.html',
  styleUrls: ['./plats-semaine.component.scss']
})
export class PlatsSemaineComponent implements OnInit {

  private arraySemaine: Array<Jour> = [];
  private arrayPlats: Array<Plat> = [];
  private addPlatForm: FormGroup;
  private currentUser: User;
  private submitted = false;
  public show: boolean = false;
  private heureLimite: Date;
  private configuration: Config;

  constructor(
    private configurationService: ConfigurationService,
    private platSemaineService: platsSemaineService,
    private platsService: PlatsService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private loadService: LoaderService,
    private notService: NotificationService
  ) { }

  ngOnInit() {
    this.show = !this.loadService.start();

      this.authenticationService.currentUser.subscribe(
        user => {
          this.currentUser = user;
          forkJoin(
            this.platSemaineService.find(),
            this.platsService.get(),
            this.configurationService.get()
          ).subscribe(
            ([semaine, plats, config]) => {
              this.arraySemaine = semaine;
              this.authenticationService.isAdmin ? this.arrayPlats = plats : '';
              this.configuration = config;
              this.show = !this.loadService.stop();
            }
          )
        }
      )

    this.addPlatForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z-éàè^ ]{1,50}$/), Validators.minLength(3)]),
    })
  }

  // BACK OFFICE
  addPlat() {
    this.submitted = true;
    if (!this.addPlatForm.valid) { 
      this.submitted = true;
      return; 
    }

    this.platsService.add(new Plat(this.addPlatForm.value.name))
      .subscribe(
        plat => {
          this.arrayPlats.push(plat);
          this.addPlatForm.reset();
          this.submitted = false;
          this.notService.success('Succés','Le plat a bien été ajouté')

        }
      )

  }

}
