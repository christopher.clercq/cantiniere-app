import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatsSemaineComponent } from './plats-semaine.component';

describe('PlatsSemaineComponent', () => {
  let component: PlatsSemaineComponent;
  let fixture: ComponentFixture<PlatsSemaineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatsSemaineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatsSemaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
