import { Component, OnInit } from '@angular/core';
import { PanierService } from '../../services/panier.service';
import { Order } from 'src/app/models/order.model';
import { MDBModalService, MDBModalRef } from 'angular-bootstrap-md';
import { LoginModalComponent } from '../modals/login-modal/login-modal.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/user.model';
import { OrderService } from 'src/app/services/order.service';
import { NotificationService } from 'src/app/services/utils/notification.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  private loginModalRef: MDBModalRef;
  private currentUser: User;
  private currentPanier:Array<Order>;
  
  constructor(
    private modalService: MDBModalService,
    private authenticationService: AuthenticationService,
    private panierService: PanierService,
    private orderService: OrderService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
    this.panierService.cartState.subscribe(cartState => this.currentPanier = cartState);
  }

  validateOrder(){
    if(!this.authenticationService.isLogged){
      this.openLoginModal();
    };

    if(!this.canOrder()) {
      this.notificationService.error('Vous n\'avez pas asser de crédit','Erreur');
      return ;
    }

    this.currentPanier.forEach(order => {
      order.date = new Date();
      order.user = this.authenticationService.currentUserValue;
      order.status = 1;
      this.orderService.add(order)
        .subscribe(() => {
          this.panierService.removeItem();
        })
    });

     
  }

  openLoginModal() {
    this.loginModalRef = this.modalService.show(LoginModalComponent);
  }

  canOrder(){
    return this.authenticationService.currentUserValue.wallet >= this.panierService.getTotal();
  }

  
}
