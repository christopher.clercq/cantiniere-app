import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { LoaderService } from 'src/app/services/utils/loader.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  private currentUser: User;
  private accountForm: FormGroup;
  public show = false;

  constructor(
    public authentificationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.show = !this.loaderService.start();
    this.authentificationService.currentUser.subscribe(
      user => {
        this.currentUser = user;
        this.show = !this.loaderService.stop();
      }
    )

    this.accountForm = this.formBuilder.group({
      mail: new FormControl('', [Validators.required, Validators.email]),
      pwd: new FormControl('', [])
    })
  }

  private submit(){
    if(!this.accountForm.valid) return;

    this.currentUser.email = this.accountForm.controls.mail.value;
    if(this.accountForm.controls.pwd.value){
      this.currentUser.password = this.accountForm.controls.pwd.value;
    } else {
      delete this.currentUser.password;
    }
    
    this.authentificationService.update(this.currentUser)
      .subscribe(
        res => {
        },
        err => {

        }
      )

  }

}
