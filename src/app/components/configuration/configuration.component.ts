import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Form } from '@angular/forms';
import { LoaderService } from 'src/app/services/utils/loader.service';
import { NotificationService } from 'src/app/services/utils/notification.service';
import { Config } from 'src/app/models/config.model';
import { forkJoin } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { MDBModalService, MDBModalRef } from 'angular-bootstrap-md';
import { UserModalComponent } from '../modals/user-modal/user-modal.component'
import { DeleteModalComponent } from '../modals/delete-modal/delete-modal.component'
import * as Fuse from 'fuse.js/dist/fuse';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  @ViewChild('f') configForm: Form;

  private limite: string;
  private maxPlats: Number;
  private config: any;
  public show: boolean;
  public users: Array<User>;
  private orderModalRef: MDBModalRef;
  public searchText: string = '';
  private previous: Array<User>;
  private fuse: Fuse<any>;
  private fuseOpts = {
    shouldSort: true,
    includeMatches: true,
    threshold: 0.1,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
      "firstName",
      "lastName",
      "email",
      "wallet"
    ]
  };

  @HostListener('input') oninput() {
    this.searchItems();
    console.log('input')
  }

  constructor(private configService: ConfigurationService,
    private loaderService: LoaderService,
    private notificationService: NotificationService,
    private userService: UserService,
    private modalService: MDBModalService) { }

  ngOnInit() {
    this.show = !this.loaderService.start();

    forkJoin(
      this.configService.get(),
      this.userService.getAll()
    ).subscribe(([config, users]) => {
      this.limite = config.limite;
      this.maxPlats = config.maxPlats;
      this.users = users;
      this.previous = users;
      this.fuse = new Fuse(users, this.fuseOpts);
      this.show = !this.loaderService.stop();
    })
  }

  searchItems() {
    if (!this.searchText || this.searchText == '') {
      this.users = this.previous;
    }

    if (this.searchText) {
      this.users = [];
      this.fuse.search(this.searchText.toString())
      .forEach(result => {
        console.log(result);
        this.users.push(result.item);
      });
    }
  }


  submit(form) {
    if (form.status != 'VALID') return;

    let config = new Config(form.value.limite, form.value.maxPlats);
    this.configService.update(config)
      .subscribe(
        () => {
          this.notificationService.info('Configuration mis à jour', 'Succés');
        }
      )
  }

  see(id: string, index: number) {
    this.orderModalRef = this.modalService.show(UserModalComponent, {
      backdrop: false,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'modal-lg',
      containerClass: '',
      animated: true,
      data: {
        id: id
      }
    });

    this.orderModalRef.content.updatedUser.subscribe((result: User) => {
      this.users[index] = result;
    })
  };

  delete(id: string, index: number) {
    this.orderModalRef = this.modalService.show(DeleteModalComponent, {
      backdrop: false,
      keyboard: false,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'modal-lg',
      containerClass: '',
      animated: true,
    });

    this.orderModalRef.content.action.subscribe((result: boolean) => {

      if (!result) { return; }

      this.userService.delete(id)
        .subscribe(
          success => {
            this.notificationService.info('Utilisateur supprimé', 'succés');
            this.users.splice(index, 1);
          }
        )
    })
  };
}
