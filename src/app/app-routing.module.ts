import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { PlatsSemaineComponent } from './components/plats-semaine/plats-semaine.component';
import { CarteComponent } from './components/carte/carte.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { RecapCommandeComponent } from './components/recap-commande/recap-commande.component';
import { PanierComponent } from './components/panier/panier.component';
import { AccountComponent } from './components/account/account.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';
import { AdminComponent } from './components/admin/admin.component';
import { AuthGuard } from './guards/auth.guards';
import { NotFoundComponent } from './components/not-found/not-found.component'
const routes: Routes = [

  // Every routes, content different between roles
  { path: '', redirectTo: 'platsSemaine', pathMatch: 'full' },
  { path: 'platsSemaine', component: PlatsSemaineComponent },

  { path: 'carte', component: CarteComponent },
  { path: 'register', component: RegisterFormComponent },
  { path: 'panier', component: PanierComponent },

  // Admin routes
  {
    path: 'admin', canActivate: [AuthGuard], data: { roles: 'Admin' },
    children: [
      { path: 'recapCommande', component: RecapCommandeComponent },
      { path: 'configuration', component: ConfigurationComponent },
    ]
  },
  // Users routes
  {
    path: 'user', canActivate: [AuthGuard], data: { roles: 'User' },
    children: [
      { path: 'account', component: AccountComponent }
    ]
  },

  { path: '404' , component: NotFoundComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
